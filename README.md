# Severino "pau pra toda obra"
Based on a popular show in Brazil, Severino is a character from [Paulo Silvino](https://pt.wikipedia.org/wiki/Paulo_Silvino) that "does it all".

This is a repo with a collection of tools that does all kinds of work, mostly for benchmarking and smoke tests.
