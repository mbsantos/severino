import Report from './report';
// import { performance } from 'node:perf_hooks';
import WikiPageSample from './sample';

export default abstract class Benchmark {
	private readonly sample: WikiPageSample;
	protected options: any;
	private readonly totalSample: any;
	private executionCounter: number;
	protected failureCounter: number;
	protected report: Report;
	private timeStart?: number;
	private timeEnd?: number;
	protected ignoreHeaders: string[];
	private chunkSize: number;

	constructor( sample, options ) {
		this.sample = sample;
		this.options = options;
		this.totalSample = this.sample.getWikiPages().length;
		this.ignoreHeaders = options.ignoreHeaders || [];
		this.executionCounter = 0;
		this.failureCounter = 0;
		this.report = new Report( {
			...options,
			totalSample: this.totalSample
		} );
		this.chunkSize = options.chunkSize || 100;
	}

	printProgress(counterSize: number = 1) {
		this.executionCounter += counterSize;
		const progress = Math.floor( ( this.executionCounter / this.totalSample ) * 100 );
		// @ts-ignore
		process.stdout.clearLine();
		process.stdout.cursorTo( 0 );
		process.stdout.write( `Running: ${progress}% (${this.executionCounter}/${this.totalSample})` );
	}

	protected abstract checkDiff( page: string ): Promise<void>;

	async executeSample( array ) {
		for ( let i = 0; i < array.length; i += this.chunkSize ) {
			const chunk = array.slice( i, i + this.chunkSize );
			await Promise.all( chunk.map( ( page ) => {
				return this.checkDiff( page );	
			} ) ).then(() => {
				this.report.savePartialReport({ lastIndex: i +	this.chunkSize + this.report.getLastIndex()});
			});
			this.printProgress(this.chunkSize);
		}
		return;
	}

	async exec() {
		const lastIndex = this.report.getLastIndex();
		this.executionCounter = lastIndex || 0;
		console.time();
		// this.timeStart = performance.now();
		const wikiPageSample = this.sample;
		const sample = wikiPageSample.getWikiPages( lastIndex );

		return this.executeSample( sample ).then( () => {
			process.stdout.write( '\n' );
			if ( this.failureCounter > 0 ) {
				console.info( `[WARNING] ${this.failureCounter} failures` );
			}
			// this.timeEnd = performance.now();
			this.report.saveFinalReport( {
				timeEnd: this.timeEnd,
				timeStart: this.timeStart,
				failureCounter: this.failureCounter
			} );
		} ).catch( (e) => {
			console.log(e)
			console.error( '[ERROR] Sample execution failed' );
		} );
	}
}
