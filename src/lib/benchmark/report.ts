import * as fs from 'fs'
import * as path from 'path'

function moduleIsAvailable( path ) {
	try {
		require.resolve( path );
		return true;
	} catch ( e ) {
		return false;
	}
}

class Report {
	private benchmarkResult: any;
	private lastIndex: number | undefined;

	constructor( options ) {
		const reportFilePath = options.reportFilePath || '../../output/results.json'
		const previousResults = moduleIsAvailable( reportFilePath ) ? require( reportFilePath ) : {};
		this.benchmarkResult = Object.assign(
			{
				diffpages: {
					default: []
				},
				sampleSize: options.totalSample,
				executionPerf: []
			},
			previousResults,
		);
	}

	appendDiffPage( page, prefix ?: string ) {
		if ( prefix ) {
			if (Array.isArray(this.benchmarkResult.diffpages[prefix])) {
				this.benchmarkResult.diffpages[prefix].push(page)
			} else {
				this.benchmarkResult.diffpages[prefix] = [page]
			}
			return
		}
		this.benchmarkResult.diffpages['default'].push(page)
	}

	getLastIndex() {
		if (this.lastIndex) {
			return this.lastIndex;
		}
		const lastIndex = fs.readFileSync(
			path.join(__dirname, '../../../output/last_index'),
			'utf8'
		)
		if (lastIndex === '') {
			return 0;
		}
		this.lastIndex = parseInt(lastIndex, 10);
		return this.lastIndex
	}

	/**
	 * Save the last index of the Sample so we can restart the script
	 * if it fails in the same position we stopped
	 * @param lastIndex 
	 */
	saveLastIndex( lastIndex: number ) {
		fs.writeFileSync(
			path.join(__dirname, '../../../output/last_index'),
			lastIndex.toString()
		)
	}

	savePartialReport({ lastIndex }: { lastIndex: number }) {
		
		this.saveLastIndex( lastIndex );
		fs.writeFileSync(
			path.join(__dirname, '../../../output/restbase-smoke-test-result.json'),
			JSON.stringify(this.benchmarkResult, null, 4)
		)
	}

	appendToReport( partialResult: any, type = 'complete' ) {
		// append to file instead of overwriting
		fs.appendFileSync(
			path.join(__dirname, `../../../output/restbase-${type}-result.json`),
			"\n" + JSON.stringify(partialResult, null, 4)
		)
	}


	saveFinalReport( executionPerf: any ) {
		this.benchmarkResult.executionPerf.push( executionPerf );
		fs.writeFileSync(
			path.join(__dirname, '../../../output/restbase-smoke-test-result.json'),
			JSON.stringify(this.benchmarkResult, null, 4)
		)
	}
}

export default Report;
