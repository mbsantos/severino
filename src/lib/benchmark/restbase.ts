import Benchmark from './index';
import { getMobileHtmlPage } from '../html';
import { detailedDiff } from 'deep-object-diff';
import { diffChars } from 'diff';

export default class RestbaseBenchmark extends Benchmark {

	async getData( page ): Promise<{ req1: any, req2: any }> {
		return {
			req1: await this.doRequest( {
				...this.options.servers[ 0 ],
				title: page,
				uri: this.options.servers[ 0 ].transformUri ?
					this.options.servers[ 0 ].transformUri( page ) :
					undefined
			} ),
			req2: await this.doRequest( {
				...this.options.servers[ 1 ],
				title: page,
				uri: this.options.servers[ 1 ].transformUri ?
					this.options.servers[ 1 ].transformUri( page ) :
					undefined
			} )
		};
	}

	async doRequest( options ) {
		try {
			return await getMobileHtmlPage( options );
		} catch ( e ) {
			console.error( '[ERROR] failure' );
			return false;
		}
	}

	bodyDiff( req1, req2 ) {
		const diff = diffChars( req1.body, req2.body );
		if ( !diff.added && !diff.removed ) {
			return false;
		} else {
			return diff;
		}
	}

	headerDiff( req1, req2 ) {
		let diff = detailedDiff( req1.headers, req2.headers );

		/**
		 * Check for headers that could update between a call and the other
		 * and lead to false positives:
		 * * age
		 * * x-cache
		 */
		this.ignoreHeaders.forEach( ( header ) => {
			delete diff.updated[ header ];
		} );

		if (
			Object.entries( diff.updated ).length === 0 &&
            Object.entries( diff.added ).length === 0 &&
            Object.entries( diff.updated ).length === 0
		) {
			return false;
		} else {
			return {
				...diff
			};
		}
	}

	async checkDiff(page) {
		try {
		  const { req1, req2 } = await this.getData(encodeURI(page)); // Request pages and their content
		  if (req1 && req2) {
			const headerDiffResults = this.headerDiff(req1, req2);
			if (headerDiffResults !== false) {
			  this.report.appendToReport({
				[page]: headerDiffResults
			  }, 'header');
			}
	
			const bodyDiffResults = this.bodyDiff(req1, req2);
			if (bodyDiffResults !== false) {
			  this.report.appendToReport({
				[page]: bodyDiffResults
			  }, 'body');
			}
		  }
		} catch (e) {
		  console.error(e);
		  this.failureCounter += 1;
		  process.stdout.write('\n');
			console.error( `[ERROR] Couldn't parse the diff for page: ${page} ` );
		}
	}
}
