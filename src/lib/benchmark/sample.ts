import { sample } from 'underscore';

export default class WikiPageSample {
	private readonly pages: any[];
	private readonly sampleSize: number;

	constructor( options ) {
		this.pages = options.data || [];
		this.sampleSize = options.sampleSize || this.pages.length;
	}


	/**
	 * 
	 * @param sliceIndex integer number to slice the sample
	 * @returns 
	 */
	getWikiPages(sliceIndex: number | undefined = undefined) {
		if ( sliceIndex ) {
			return this.pages.slice( sliceIndex, sliceIndex + this.sampleSize );
		}
		return this.pages;
	}

	getSample() {
		return sample( this.getWikiPages(), this.sampleSize );
	}
}
