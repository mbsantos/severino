import throttledRequest from './request-utils';

export const getHtmlPage = async ( { hostname, title, lang, project } ) => {
	const uriHostname = hostname || `${lang}.${project}.org`;
	const uri = `https://${uriHostname}/api/rest_v1/page/html/${title}`;
	return new Promise( ( resolve ) => {
		throttledRequest( { method: 'GET', uri, encoding: null }, function ( err, res ) {
			if ( err ) {
				console.error( err );
				return {};
			}
			resolve( res );
		} );
	} );
};

export const getMobileHtmlPage = async ( { hostname, title, lang, project, uri, headers } ) => {
	const uriHostname = hostname || `${lang}.${project}.org`;
	uri = uri ?? `https://${uriHostname}/api/rest_v1/page/mobile-html/${title}`;
	return new Promise( ( resolve ) => {
		throttledRequest( { method: 'GET', uri, headers }, function ( err, res ) {
			if ( err ) {
				console.error( err );
				resolve( {} );
			}
			resolve( res );
		} );
	} );
};
