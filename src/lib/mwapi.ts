import throttledRequest from './request-utils';

export const get = async ( params ) => {
	const result = await new Promise( ( resolve ) => {
		throttledRequest( { method: 'GET', ...params }, function ( err, res ) {
			if ( err ) {
				console.error( err );
				resolve( {} );
			}
			resolve( res );
		} );
	} );

	// @ts-ignore
	return JSON.parse( result.body );
};
