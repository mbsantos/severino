import request from 'request';

const throttledRequest = require( 'throttled-request' )( request );
const throttleConfig = {
    requests: 15,
    milliseconds: 500
};
throttledRequest.configure( throttleConfig );
export default throttledRequest
