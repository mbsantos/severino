import minimist from 'minimist';

export const run = ( callback ) => new Promise( ( resolve, reject ) => {
	console.info( '[INFO] Script started' );
	callback(
		minimist( process.argv.slice( 2 ) )
	).then( result => {
		resolve( result );
	} ).catch( reject );
} ).then( () => {
	console.info( '[INFO] Script finished' );
} );

exports.default = {
	run
};
