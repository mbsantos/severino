export const normalizeTitle = ( title ) => {
	return title.replace( / /g, '_' );
};
