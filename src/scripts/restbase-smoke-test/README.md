# How to run the script
**This suite is not well suited to be run in the WMF infrastructure because of the nodejs version used**

## Create a benchmark sample
```shell
 npm run script ./scripts/restbase-smoke-test/generateSample.ts -- --lang=en
```

## Run the main script
```shell
npm run script ./scripts/restbase-smoke-test/index.ts
```

## Tunneling to internal PCS
In order to test against mobileapps internal connections, make sure to tunnel your connection through ssh:

```shell
ssh -L 4102:mobileapps.discovery.wmnet:4102 mwmaint1002.eqiad.wmnet
```

And if you are doing it, make sure that you disable nodejs TLS checks:
```shell
NODE_TLS_REJECT_UNAUTHORIZED=0 npm run script ./scripts/restbase-smoke-test/index.ts
```