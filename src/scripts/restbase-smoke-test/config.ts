'use strict';

export const getOptions = ( { lang = 'en', project = 'wikipedia' } ) => {
	const sampleArray = require( `../../fixtures/${lang}wiki-sample.json` );
	return {
		reportFilePath: '../../output/restbase-smoke-test-results.json',
		sample: {
			data: sampleArray
		},
		benchmark: {
			cutSampleHead: 10,
			ignoreHeaders: [
				'age',
				'x-cache',
				'x-cache-status',
				'x-client-ip',
				'date',
				// TODO: should we really ignore set-cookie?
				'set-cookie',
				'server-timing',
				'server',
				'etag',
				'x-envoy-upstream-service-time'
			],
			servers: [
				{
					label: 'Restbase',
					project,
					lang,
					params: {},
					headers: {} // a custom header can be passed to every request
				},
				{
					label: 'Restbase (compat)',
					project,
					lang,
					transformUri: ( page ) => {
						console.log( page );
						// return `https://mobileapps.discovery.wmnet:4102/${lang}.wikipedia.org/v1/page/mobile-html/${page}`;
						return `https://staging.svc.eqiad.wmnet:4113/${lang}.wikipedia.org/v1/page/mobile-html/${page}`;
					},
					headers: {
						'x-restbase-compat': 'true',
						'X-Use-MW-REST-Parsoid': 'true'
					}
				}
			]
		}
	};
};
