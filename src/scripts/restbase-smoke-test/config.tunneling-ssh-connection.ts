'use strict';

export const getOptions = ( { lang = 'en', project = 'wikipedia' } ) => {
	const sampleArray = require( `../../fixtures/${lang}wiki-sample.json` );
	return {
		reportFilePath: '../../output/restbase-smoke-test-results.json',
		sample: {
			data: sampleArray
		},
		benchmark: {
			ignoreHeaders: [
				'age',
				'x-cache',
				'x-cache-status',
				'x-client-ip',
				'date',
				// TODO: should we really ignore set-cookie?
				'set-cookie',
				'server-timing',
				'server',
				'etag',
				'x-envoy-upstream-service-time'
			],
			servers: [
				{
					label: 'Restbase',
					project,
					lang,
					params: {},
					headers: {} // a custom header can be passed to every request
				},
				{
					label: 'API Gateway',
					project,
					lang,
					transformUri: ( page ) => {
						return `https://mobileapps.discovery.wmnet:4102/en.wikipedia.org/v1/page/mobile-html/${page}`;
					},
					headers: {
						'x-restbase-compat': 'true',
						'X-Use-MW-REST-Parsoid': 'true'
					}
				}
			]
		}
	};
};
