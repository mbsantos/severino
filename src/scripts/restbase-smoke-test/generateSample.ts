'use strict';

import { run } from '../../lib/script-utils';
import { get } from '../../lib/mwapi';
import * as fs from 'fs';
import path from 'path';
import { normalizeTitle } from '../../lib/titles';

const generateSample = async ( lang: string, project: string ): Promise<any[]> => {
	const uri = `https://${lang}.${project}.org/w/api.php`;
	const queryParams = {
		format: 'json',
		action: 'query',
		generator: 'random',
		grnnamespace: 0,
		rvprop: 'content',
		grnlimit: 500
	};
	return get( {
		uri,
		qs: queryParams
	} ).then( ( { query } ) => {
		const { pages } = query;

		return Object.values( pages ).map( ( page: any ) => {
			return normalizeTitle( page.title );
		} );
	} );
};

run(
	async ( args ) => {
		const { lang = 'en', project = 'wikipedia', sampleSize = 500 } = args;
		const numberOfRequests = Math.ceil( sampleSize / 500 );
		const sample: any[] = [];

		for ( let i = 0; i < numberOfRequests; i++ ) {
			const sampleSlice = await generateSample( lang, project );
			sample.push( ...sampleSlice );
			// print progress
			console.info( `[INFO] ${i + 1} / ${numberOfRequests} requests completed` );
		}

		// make sample unique
		const uniqueSample = Array.from( new Set( sample ) );

		fs.writeFileSync(
			path.join( __dirname, `../../fixtures/${lang}wiki-sample.json` ),
			JSON.stringify( uniqueSample, null, 4 )
		);
		console.info( '[INFO] Sample Saved' );
	}
);
