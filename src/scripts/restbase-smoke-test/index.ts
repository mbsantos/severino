'use strict';

import WikiPageSample from '../../lib/benchmark/sample';
import { run } from '../../lib/script-utils';
import RestbaseBenchmark from '../../lib/benchmark/restbase';
import { getOptions } from './config';

run(
	( args ) => {
		const options = getOptions( args );
		let sample = new WikiPageSample( options.sample );
		let benchmark = new RestbaseBenchmark( sample, options.benchmark );
		return benchmark.exec();
	}
);
